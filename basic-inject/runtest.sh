#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/mcelog/Regression/basic-inject
#   Description: basic mcelog inject decoding
#   Author: Andrej Manduch <amanduch@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2015 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="mcelog"
LOG_FILE=`mktemp`
LOGGING_PID=""
LOGGING_STATUS="stopped"

MAJOR_RELEASE=$(cat /etc/redhat-release | grep -o '[[:digit:]]\+\.\?[[:digit:]]*' | sed 's/\.[[:digit:]]*$//g') || rlDie "could not parse /etc/redhat-release"

_mcelog_start_logging()
{
	if [ $LOGGING_STATUS != "stopped" ]; then
		return 1
	fi

	if [ $MAJOR_RELEASE -ge 7 ]; then
		journalctl -fu mcelog -n0 -o cat > $LOG_FILE &
	else
		tail -f /var/log/mcelog -n0 > $LOG_FILE &
	fi

	ret=$?
	sleep 1
	LOGGING_PID=`jobs -p | tail -n 1`
	LOGGING_STATUS="logging"

	return $ret
}

_mcelog_clear_logging()
{
	echo "" > $LOG_FILE

	return 0
}

_mcelog_stop_logging()
{
	if [ $LOGGING_PID != "" ]; then	
		return
	fi

	kill $LOGGING_PID
	return 0
}

_kill_mcelog()
{
	service mcelogd stop  > /dev/null 2>&1
	service mcelog stop  > /dev/null 2>&1 # it's mcelogd on RHEL6 and mcelog on RHEL7
	sleep 1
	killall -9 mcelog > /dev/null 2>&1
}

# return 0 if success
#        1 if fail
_get_mce-inject()
{
	if [ ! -d mce-inject ]; then
		git clone http://git.kernel.org/pub/scm/utils/cpu/mce/mce-inject.git/
	fi

	pushd mce-inject
	if [ $? -ne 0 ]; then
		return 1
	fi

	make
	modprobe -v mce-inject
	popd

	return $?
}

_start_mcelog()
{
	if [ $MAJOR_RELEASE -ge 7 ]; then
		systemctl start mcelog.service
	else
		service mcelogd start
	fi

	return $?
}

_stop_mcelog()
{
	if [ $MAJOR_RELEASE -ge 7 ]; then
		systemctl start mcelog.service
	else
		service mcelogd start
	fi

	ret=$?
	sleep 1
	killall -9 mcelog 2>/dev/null >/dev/null
	killall -9 mcelogd 2>/dev/null >/dev/null

	return $ret
}


rlJournalStart

    mcelog --is-cpu-supported 2>&1 >/dev/null
	if [ "$?" -ne "0" ] ; then
		rlLogInfo "host CPU is not supported by mcelog, skipping this test set"
		rhts-report-result $TEST SKIP $OUTPUTFILE
		rlJournalEnd
		exit 0
	fi

    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
	cp ./test-simple-corrected $TmpDir
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStart FAIL "basic mce-inject corrected scenario"
	res="pass"
	_get_mce-inject
	_start_mcelog
	_mcelog_start_logging
	./mce-inject/mce-inject "./test-simple-corrected"
	sleep 1
	grep  -iq "deadbeef" $LOG_FILE
	if [ $? -ne 0 ]; then
		res="fail"
	fi

	grep -iq "corrected" $LOG_FILE
	if [ $? -ne 0 ]; then
		res="fail"
	fi

	if [ $res == "fail" ]; then
		rlFail "corrected scenario failed"
	else
		rlPass
	fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
	rlRun "rm $LOG_FILE" 0 "Removing temp log FILE"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
