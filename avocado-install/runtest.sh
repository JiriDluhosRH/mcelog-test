#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/mcelog/avocado-install
#   Description: Installs
#   Author: Andrej Manduch <amanduch@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2017 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="mcelog"

if grep -q "release 6" /etc/redhat-release ; then
    REDHAT_RELEASE="6"
else
    if grep -q "release 7" /etc/redhat-release ; then
        REDHAT_RELEASE="7"
    else
        if grep -q "release 8" /etc/redhat-release ; then
            REDHAT_RELEASE="8"
        else
            echo "Could not identify RHEL release"
            exit 3
        fi
    fi
fi

rlJournalStart
    rlPhaseStartSetup
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStart FAIL "Avocado installation"
	
	if [ "$REDHAT_RELEASE" == "6" ] ; then
		rlRun "yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm"
	else
        if [ "$REDHAT_RELEASE" == "7" ] ; then
            rlRun "yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm"
        else
            rlRun "yum install -y python2-pip"
        fi
	fi

    if [ "$REDHAT_RELEASE" == "6" ] || [ "$REDHAT_RELEASE" == "7" ] ; then
    	rlRun "yum install -y python2-avocado"
    	rlRun "yum install -y python2-avocado-plugins-varianter-yaml-to-mux.noarch"
    else
        rlRun "pip2 install avocado-framework"
        rlRun "pip2 install avocado-framework-plugin-varianter-yaml-to-mux"
    fi

    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
