#!/bin/bash

# Copyright (c) 2006 Red Hat, Inc. All rights reserved. This copyrighted material 
# is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General
# Public License v.2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# Author: Jan Tluka

SERVICE=mcelog

. /usr/bin/rhts-environment.sh
. /usr/share/rhts-library/rhtslib.sh

rlJournalStart

	mcelog --is-cpu-supported 2>&1 >/dev/null
	if [ "$?" -ne "0" ] ; then
		rlLogInfo "host CPU is not supported by mcelog, skipping this test set"
		rhts-report-result $TEST SKIP $OUTPUTFILE
		rlJournalEnd
		exit 0
	fi

    rlPhaseStartSetup "Prepare"
        rlServiceStop $SERVICE
    rlPhaseEnd

    rlPhaseStartTest "Mandatory actions"
        rlAssertExists "/usr/lib/systemd/system/mcelog.service"
    rlPhaseEnd

    rlPhaseStartTest "Start"
        rlRun "systemctl start $SERVICE" 0
        rlRun "systemctl status $SERVICE" 0
        rlRun "systemctl start $SERVICE" 0
        rlRun "systemctl status $SERVICE" 0
        rlRun "systemctl restart $SERVICE" 0
        rlRun "systemctl status $SERVICE" 0
    rlPhaseEnd

    rlPhaseStartTest "Stop"
        rlRun "systemctl stop $SERVICE" 0
        rlRun "systemctl status $SERVICE" 3
        rlRun "systemctl stop $SERVICE" 0
        rlRun "systemctl status $SERVICE" 3
    rlPhaseEnd

    rlPhaseStartTest "Lock file"
        rlLogInfo "Skipping lock file test, seems that mcelog does not use lock file anymore"
        # rlRun "touch /var/lock/subsys/mcelog"
        # rlRun "systemctl status $SERVICE" 2
        # rlRun "rm -f /var/lock/subsys/mcelog"
    rlPhaseEnd

    rlPhaseStartTest "PID file"
		rlLogInfo "Skipping PID file test, mcelog service doesn't use PID file"
#        rlRun "touch /var/run/mcelog.pid"
#        rlRun "service $SERVICE status" 1
    rlPhaseEnd

    rlPhaseStartTest "Condrestart"
        service $SERVICE | grep "condrestart" && COND="1" || COND="0"
        if [ "$COND" -eq "1" ]; then
            rlRun "systemctl condrestart $SERVICE" 0
            rlRun "systemctl status $SERVICE" 0
        else
            rm -f /var/lock/subsys/$SERVICE
        fi
        service $SERVICE stop
    rlPhaseEnd

    rlPhaseStartTest "Invalid arguments"
        rlRun "service $SERVICE" 2
    rlPhaseEnd

    rlPhaseStartCleanup "Restore"
        rlServiceRestore $SERVICE
    rlPhaseEnd

rlJournalPrintText
