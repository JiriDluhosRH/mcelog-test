#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /kernel/mcelog/bz1355847-loading-EDAC-breaks-mcelog
#   Description: Test for BZ#1355847 (loading EDAC breaks mcelog)
#   Author: Andrej Manduch <amanduch@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="mcelog"
EINJ_AVAILABLE="n"

load_einj(){
        local result="PASS"

        rlPhaseStart FAIL "einj module loading"

                modprobe einj
                if [ $? -eq 0 ]; then
                        rlPass "einj was sucessfully loaded"
                else
                        rlFail "can't load einj read comments bellow"
                        rlLog "There is probablity that you forgot to enable einj in firmware settings"
                        rlLog "It shoul'd be somewher under WHEA->Error Injection Support"
                        result="FAIL"
                fi

                mount -t debugfs none /sys/kernel/debug/

                # now we check if all files that we need are availibe
                if [ ! -f "/sys/kernel/debug/apei/einj/error_inject" ] ||
                   [ ! -f "/sys/kernel/debug/apei/einj/error_type" ] ||
                   [ ! -f "/sys/kernel/debug/apei/einj/param1" ] ||
                   [ ! -f "/sys/kernel/debug/apei/einj/param2" ] ; then
                        result="FAIL"
                        rlFail "Some of necessary files was not availible, check your "\
                                "firmware if error injection is set correctly"
                fi

                if [ -f "/sys/kernel/debug/apei/einj/available_error_type" ]; then
                        grep "0x00000008" /sys/kernel/debug/apei/einj/available_error_type -q
                        if [ $? -ne 0 ]; then
                                rlFail "einj doesn't seems to support Memory Correctable"\
                                       " injection, check firmware settings"
                                result="FAIL"
                        fi
                fi
        rlPhaseEnd

        if [ $result != "PASS" ]; then
                return 1
        else
                EINJ_AVAILABLE="y"
                return 0
        fi
}

# Params:
#        $1 - addr - address on which error should be injected
_einj_inject_error(){
        local addr=$1

        rlLog "Injecting Correctable error in to $addr"
        echo $addr > /sys/kernel/debug/apei/einj/param1
        echo 0xffffffffffffffff > /sys/kernel/debug/apei/einj/param2
        echo 0x08 > /sys/kernel/debug/apei/einj/error_type
        echo 1 > /sys/kernel/debug/apei/einj/error_inject 2>/dev/null
}

_mcelog_start(){
	case $(uname -r) in
		*el6*)
			rlServiceStart mcelogd
			;;
		*el7*)
			rlServiceStart mcelog
			;;
		*)
			rlWarn "Unknown OS"
			service mcelogd start > /dev/null 2>&1
			service mcelog start > /dev/null 2>&1
			;;
	esac

	sleep 1
}

_did_mcelog_get_something(){
	mcelog --raw | grep "ADDR" -q
	if [ $? -eq 0 ]; then
		rlLog "mcelog senses some error"
		return 0
	else
		rlFail "mcelog didn't catch anything"
		return 1
	fi
}

export addresses="0x100 0x1000 0x10000 0xdead 0xdeadbe 0xb16b00"

rlJournalStart
rlPhaseStartSetup
	rlAssertRpm $PACKAGE
	rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
	rlRun "pushd $TmpDir"
rlPhaseEnd

_mcelog_start
load_einj
rlPhaseStartTest
    if [ "$EINJ_AVAILABLE" == "y" ] ; then
    	ret=PASS
    	for addr in $addresses;
    	do
    		rlLog "trying to inject and sence by mcelog error on $addr"
    		_einj_inject_error $addr
    		_did_mcelog_get_something
    		if [ $? -ne 0 ]; then
    			ret=FAIL
    		fi
    	done

    	if [ $ret == "PASS" ]; then
    		rlPass "looks like mcelog works ok with edac loaded"
    	fi
    else
        rlFail "einj is not available or not properly initialized, can't test"
    fi
rlPhaseEnd

rlPhaseStartCleanup
	rlRun "popd"
	rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
rlPhaseEnd
rlJournalPrintText
rlJournalEnd
