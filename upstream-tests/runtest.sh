#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/mcelog/Regression/upstream-tests
#   Description: mcelog upstream tests port
#   Author: Andrej Manduch <amanduch@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2015 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see https://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="mcelog"
mceinjectprefix=`mktemp -d`
FILES="0001-Beaker-preping.patch"

_get_mcelog_git()
{
	if [ ! -d mcelog ]; then
		git clone https://github.com/andikleen/mcelog
	else
		return 3
	fi

	return $?
}

_install_mceinject()
{
	if [ ! -d mce-inject ]; then
		git clone https://git.kernel.org/pub/scm/utils/cpu/mce/mce-inject.git/
	fi

	which page-types > /dev/null 2>&1
	if [ $? -ne 0 ];then
		git clone https://github.com/andikleen/mce-test.git
		pushd mce-test
		pushd tools
		make install
		popd
		cp bin/* /usr/local/bin/
		popd
	fi

	pushd mce-inject
	prefix=$mceinjectbin make
	prefix=$mceinjectbin make install

	export PATH=$PATH:$mceinjectprefix/bin
	popd
}

#
# $1 - relativa/absolute path to mcelog source
_patch_mcelog_tests()
{
	pushd $1
	git checkout -b beaker-preped
	git am ./0001-Beaker-preping.patch
	popd
}

rlJournalStart

	mcelog --is-cpu-supported 2>&1 >/dev/null
	if [ "$?" -ne "0" ] ; then
		rlLogInfo "host CPU is not supported by mcelog, skipping this test set"
		rhts-report-result $TEST SKIP $OUTPUTFILE
		rlJournalEnd
		exit 0
	fi

    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
	rlRun "cp $FILES $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest "Upstream tests"
	MCE_DIR="./"
	_get_mcelog_git
	_install_mceinject
	cp ./0001-Beaker-preping.patch mcelog/
	pushd mcelog
	_patch_mcelog_tests $MCE_DIR
	make test
	popd
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
	rm mceinjectprefix -rf
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
