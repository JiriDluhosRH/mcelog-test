#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/mcelog/avocado-suite
#   Description: Installs
#   Author: Andrej Manduch <amanduch@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2017 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="mcelog"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
	rlRun "cp -rf * $TmpDir"
        rlRun "pushd $TmpDir"
	#load sepolicy so triggers can be properly executed
	checkmodule -M -m -o triggers_policy.mod scripts/triggers_policy.te
	semodule_package -o triggers_policy.pp -m triggers_policy.mod
	semodule -i triggers_policy.pp
	#TODO remove next line if possible
	setenforce 0
    rlPhaseEnd

    rlPhaseStartTest "Avocado triggers test"
	cp triggers.py.data/mcelog-el7-light-triggers.conf /etc/mcelog/mcelog.conf
	rlRun "avocado run ./triggers.py --mux-yaml ./triggers-rhel7.mux --xunit ./triggers1-xunit.xml"
	rlRun "avocado run ./sanity.py --xunit ./sanity-xunit.xml"
	./merge_junit_results.py ./triggers1-xunit.xml ./sanity-xunit.xml > junix.xml
	rlFileSubmit ./junit.xml
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
#        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
