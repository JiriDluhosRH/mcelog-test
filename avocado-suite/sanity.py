from avocado import Test
from avocado import main
from avocado.utils import process

from difflib import SequenceMatcher

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

class Sanity(Test):

#	@avocado.skip("RHBZ#1481421 RHBZ#1454419")
	def testHelp(self):
		proc = process.run("mcelog --help", ignore_status=True, verbose=False)
		str_help = open(self.datadir + "/str_help", 'r').read()
		ratio = similar(proc.stdout, str_help)
		if ratio < 0.8:
			self.log.warn("stdout doesn't contain help message")
			ratio = similar(proc.stderr, str_help)
			if ratio < 0.8:
				self.fail("stderr doesn't contain help message")

		self.assertFalse(proc.exit_status, 0)

#	@avocado.skip("RHBZ#1481421")
	def testVersion(self):
		proc = process.run("mcelog --version")
		out = proc.stdout + proc.stderr
		ver = process.run("rpm -qa mcelog").stdout
		ratio = similar(out, ver)
		if ratio < 0.6:
			self.fail("mcelog --version seems to be outputing something wrong")

#	@avocado.skip("RHBZ#1481421")
	def testCPUSupport(self):
		with open(self.datadir + "/cpu_list") as fp:
			for line in fp:
				cpu_name = line.rstrip()
				print cpu_name
				run_cmd = "mcelog --is-cpu-supported --cpu " + cpu_name
				print ("checking if " + run_cmd + " wil fail")
				process.run(run_cmd)
