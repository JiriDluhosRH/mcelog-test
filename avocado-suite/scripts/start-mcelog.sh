#!/bin/bash

if grep -q -i "release 7" /etc/redhat-release; then
	systemctl stop mcelog
	sleep 3
	pgrep mcelog >/dev/null
	if [ ! $? ] ; then
		killall -9 mcelog
		sleep 3
	fi

	systemctl start mcelog
	else

	service mcelogd stop
	sleep 3
	pgrep mcelog >/dev/null
	if [ ! $? ] ; then
		killall -9 mcelog
		sleep 3
	fi

	service mcelogd start
fi
