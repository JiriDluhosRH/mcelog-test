#!/bin/bash

# $1 - address
# $2 - type (default = 0x8)

modprobe -i einj > /dev/null
echo $1 > /sys/kernel/debug/apei/einj/param1
if [ "x$2" == "x" ]; then
	etype="0x8"
else
	etype=$2
fi

echo $etype > /sys/kernel/debug/apei/einj/error_type
echo $((-1 << 12)) > /sys/kernel/debug/apei/einj/param2
echo 1 > /sys/kernel/debug/apei/einj/error_inject
