#!/bin/bash

export inject_path=$1

if [ ! -f "$inject_path" ]; then
	echo "Usage: $0 path/to/mce/inject/file.txt"
	echo
	echo "path was not provided, using default one"
	inject_path="basic-inject.txt"
fi

cat $inject_path | mce-inject
