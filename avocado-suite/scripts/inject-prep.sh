#!/bin/bash

run_or_die() {
	$@
	if [ $? -ne 0 ]; then
		echo "command \'$@\' failed, exiting..."
		exit 1
	fi
}

modprobe -r sb_edac >/dev/null
modprobe -r edac_core >/dev/null

run_or_die modprobe mce-inject
