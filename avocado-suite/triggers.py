import os
import platform
import avocado
from avocado import Test
from avocado import main
from avocado.utils import process
import shutil
import json
import re

def isInteger(s):
	try: 
		int(s)
		return True
	except ValueError:
		return False

def strmatch(pattern, string):
	m = re.match(pattern, string)
	if m == None:
		return False
	else:
		return True

import subprocess
import threading

class Victim():
	_victim_subprocess = None
	_physical_address = '0x0'
	_virtual_address = '0x00'
	_page_replaced = False
	_victim_reading = threading.Thread()
	_big_lock = threading.Lock()

	def __init__(self):
		self.start()

	def get_virtual_address(self):
		self._big_lock.acquire()
		ret = self._virtual_address
		self._big_lock.release()
		return ret

	def get_physical_address(self):
		self._big_lock.acquire()
		ret = self._physical_address
		self._big_lock.release()
		return ret

	def get_page_replaced(self):
		self._big_lock.acquire()
		ret = self._page_replaced
		self._big_lock.release()
		return ret

	def reset_page_replaced(self):
		self._big_lock.acquire()
		self._page_replaced = False
		self._big_lock.release()

	def _set_addresses(self):
		for line in iter(self._victim_subprocess.stdout.readline, ''):
			print line
			match = re.search(r'physical address of \(([0-9a-fx]+)\) = ([0-9a-fx]+)',
			                  line)
			if match:
				self._big_lock.acquire()
				self._physical_address = match.group(2)
				self.__virtual_address = match.group(1)
				print "New physical address was aquired: " + self._physical_address
				self._big_lock.release()
			else:
				match = re.search(r'Page was replaced. New physical address = ([0-9a-fx]+)', line)
				if match:
					self._big_lock.acquire()
					self._page_replaced = True
					self._physical_address = match.group(1)
					print "New physical address was aquired: " + self._physical_address
					self._big_lock.release()


	def start(self):
		self._victim_subprocess = subprocess.Popen(["./scripts/victim/victim",
		'-p'], stdout=subprocess.PIPE)
		self._victim_reading = threading.Thread(target = self._set_addresses)
		self._victim_reading.start()


class MCEPageErrorTrigger(Test):
	victim = None

	def once_in_a_row(affected_function):
		def wrapper(self):
				if not os.path.exists(os.path.join(self.teststmpdir, "once-in-row")):
					open(os.path.join(self.teststmpdir,
					"once-in-row"),"w").close()
	
				f = open(os.path.join(self.teststmpdir, "once-in-row"), "r+")
				if f.read() == affected_function.__name__:
					"""do nothing"""
					result = None
				else:
					f.seek(0)
					f.truncate()
					f.seek(0)
					f.write(affected_function.__name__)
					result = affected_function(self)
	
				f.close()
				return result
		return wrapper

	def copy_triggers(self):
		triggers = ['page-error', 'dimm-error', 'bus-error', 'cache-error',
		'iomca-error', 'socket-memory-error', 'unknown-error']
		_, dist_ver, _ = platform.linux_distribution()

		if dist_ver < 7:
			trigger_prefix = "/etc/mcelog/"
		else:
			trigger_prefix = "/etc/mcelog/triggers/"

		for trigger in triggers:
			shutil.copy2(self.datadir + "/generic-error-trigger.local",
			trigger_prefix + trigger + "-trigger.local")

	def test1(self):
		trigger_logs_path_prefix = "/tmp/mcetriggers/"
		test_var = self.params.get("var")
		test_condition = self.params.get("test")
		trigger_name = self.params.get("trigger")
		self.log.info("Trigger name is: %s, variabile under testing is: %s, and testing condition is: %s", trigger_name, test_var, test_condition)

		trigger_logs_path_prefix = "/tmp/mcetriggers/"
		trigger_logs = self.params.get("expected-triggers")

		""" Test if trigger was fired: """
		if os.path.isfile(trigger_logs_path_prefix + trigger_name):
			self.log.info(trigger_name + ' trigger was expected and triggered')
		else:
			self.fail(trigger_name + 'trigger wasn\'t triggered')
			return

		""" Test if variabile exists and have sane value: """
		with open(trigger_logs_path_prefix + trigger_name) as data_file:
			data = json.load(data_file)

		file_var = data.get(test_var)
		var = file_var;
		exec("result = " + str(test_condition), globals(), locals())
		self.log.debug("return of eval was: %s", str(result))
		if result:
			self.log.info('PASS more info TBD')
		else:
			self.log.info('test for %s, with var: %s test: %s FAILED',
			trigger_name, test_var, test_condition)
			self.fail('for details look at info message above ^^')

	""" rly have no idea how to call this one, it's just thing that it's run
	before most (if not every) setUp method """
	def setUpSetUp(self):
		self.victim = Victim()
		self.copy_triggers()

		shutil.rmtree('/tmp/mcetriggers/', ignore_errors = True)
		os.mkdir('/tmp/mcetriggers')
		os.chmod('/tmp/mcetriggers', 0777)

	@once_in_a_row
	def setUpEinj(self):
		self.setUpSetUp()
		process.system("./scripts/start-mcelog.sh", timeout=90)
		for _ in range (self.params.get('inject_count', default=20)):
			einj_inject_command = "./scripts/einj-inject.sh "
			einj_inject_command += str(self.victim.get_physical_address())
			process.system(einj_inject_command, timeout=20)

	@once_in_a_row
	def setUpMceInject(self):
		self.setUpSetUp()
		process.system("./scripts/inject-prep.sh")
		process.system("./scripts/start-mcelog.sh", timeout=90)

		inject_file = self.params.get('file', default='page-error.txt')
		self.log.debug('File with inject params is: ' + inject_file)
		inject_template =  open(self.datadir + '/' + inject_file, 'r').read()
		for _ in range (self.params.get('inject_count', default=20)):
			inject = re.sub('<VICTIM_ADDR>', self.victim.get_physical_address(),
			         inject_template)
			mceinject_proc = subprocess.Popen('mce-inject',
			                                  stdin=subprocess.PIPE)
			mceinject_proc.communicate(input=inject)
			mceinject_proc.wait()

	def setUp(self):
		trg_mechanism = self.params.get("inject-type")
		self.log.info("requested trigger meghanism is: " + trg_mechanism)
		if trg_mechanism == "mce-inject":
			self.setUpMceInject()
		elif trg_mechanism == "einj":
			self.setUpEinj()
		else:
			raise NotImplemented
